.. jsonmetamodel documentation master file, created by
   sphinx-quickstart on Thu Jun 18 09:22:50 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to jsonmetamodel's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   sql_data_types

jsonmetamodel.metaclass
=======================

.. automodule:: jsonmetamodel.metaclass

jsonmetamodel.fieldtypes
========================

.. automodule:: jsonmetamodel.fieldtypes

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
